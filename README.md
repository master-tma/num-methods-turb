# Numerical Methods Turbulence

Repository for the course "Advanced Numerical Methods" of the TMA Master (Turbulence,
Methods and Applications) at UGA.

The
[website of the course](https://master-tma.gricad-pages.univ-grenoble-alpes.fr/num-methods-turb/intro.html)
is online.

## For writers

Install the requirements for building the "book" with

```
pip install -r book/requirements.txt
python3 -m bash_kernel.install
```

mdformat and black are used to format the sources of the book (command `make format`).
These packages can be installed for example with `pipx`.

```
pip install pipx
pipx install mdformat-myst --include-deps
pipx install black
```
