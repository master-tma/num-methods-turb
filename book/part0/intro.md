# Introduction

To study problems related to turbulence with computers, one needs to compute and to
develop code. It is good to now enough about how to install software and libraries to be
relatively independent with the computers that you are going to use. During these
course, we will learn some methods to install programs that will be useful in general
and in particular for this course.

We will use Python 3 (with Miniconda3), a good Python IDE (either Spyder or VSCode),
Jupyter and a version-control tool (Mercurial, or Git if you know it and really like
it).

## Linux, and the operative system of your computer

Moreover, let's add that the best OS for HPC (and HPC with Python) is Linux/GNU. Windows
(at least without [WLS](https://en.wikipedia.org/wiki/Windows_Subsystem_for_Linux)) and
even macOS are less adapted for this particular application. Python is a cross-platform
language but nevertheless, you will get a better experience for HPC with Python on
Linux. Moreover, all large HPC clusters are running on Linux. Therefore, we will first
focus on learning the basis of Linux and how to setup a good development and computing
environment for this OS.

However, most students own computers running on Windows, so we will also spend a bit of
time to learn how to setup a good environment on Windows (however using [WSL]).

[wsl]: https://fr.wikipedia.org/wiki/Windows_Subsystem_for_Linux
