#!/usr/bin/env python
"""
Launch the script simul_ns3d_forced_isotropic.py

"""

from fluiddyn.clusters.mesonet import Zen as Cluster

cluster = Cluster()

cluster.commands_setting_env = """
module load miniforge3
conda activate env-fluidsim
"""
# conda-forge MPI uses mpirun and not srun (from Slurm)
cluster.cmd_run = "mpirun"

cluster.submit_command(
    "python simul_ns3d_forced_isotropic.py --nx 96 --t_end 8",
    name_run="fld_example",
    nb_mpi_processes=4,
    omp_num_threads=1,
)
