# First steps with a supercomputer with Mesonet

MesoNET est une fédération de mésocentres de calcul. Les mésocentres sont des structures
publiques (souvent des Unités Mixte de Service) dédiées à fournir des moyens de calculs
tels que des clusters de calcul.

We are going to use a MesoNET supercomputer, so let's see how it works. There are
different websites associated with MesoNET:

- The main website: https://www.mesonet.fr/

- The user documentation: https://www.mesonet.fr/documentation/user-documentation/

- Account creation/management: https://iam.mesonet.fr/

- Management of projects and logins/ssh keys to log on machines:
  https://acces.mesonet.fr

The steps to use MesoNET services are:

1. Create an account via https://iam.mesonet.fr/

2. Reply to the mail invitation by clicking on the link

3. Login on https://acces.mesonet.fr to manage your logins/ssh keys associated with your
   account and project (see just below)

## Setup your account and connect to Zen

[Zen](https://www.mesonet.fr/documentation/user-documentation/code_form/zen/) is a
supercomputer hosted in Lille. Our goal is to be able to login on the login node of this
supercomputer.

You need to create a ssh key and upload it to the website. Associate the ssh key with a
MesoNET supercomputer (Zen in our case). All this is explained in details
[here](https://www.mesonet.fr/documentation/user-documentation/acces/ssh).

After few hours, the public key that you uploaded on the website should have been copied
in the ~/.ssh directory on the login node of Zen, meaning that you can login with ssh
with the login associated with your project and supercomputer (Zen):

You need to copy this login from the website https://acces.mesonet.fr and you can
finally run (more details
[here](https://www.mesonet.fr/documentation/user-documentation/code_form/zen/connexion)):

```sh
ssh -X m24075-???@zen.univ-lille.fr
```

You should be connected on the login node!

## Presentation of Zen, an example of supercomputer

The technical description of Zen can be found
[here](https://www.mesonet.fr/documentation/user-documentation/code_form/zen/description).

A supercomputer is a set of nodes (individual computers) connected through a network. In
practice, these computers are not like standard personal computers or laptops but
stacked into racks.

In Zen, there are 70 computing nodes (2 CPUs AMD EPYC 9534, 64 cores/CPU, 384 Go RAM
DDR5), 2 similar nodes with a lot of memory (3 To!) and 1 similar visualization node
with 2 GPUs.

To launch programs on these nodes, one needs to submit "jobs" via a program called a
scheduler. For Zen, the scheduler is Slurm.

In supercomputer, there are one or few special nodes called login nodes ("frontale" in
French) used for connection, environment preparation (compilation) and job submissions.

## Setup a computing environment

Zen works with modules, which can be used to load environments to use different
libraries and programs.

```sh
module avail
```

which gives something like:

```
--------------------------------- /bxfs/modules/core ----------------------------------
   aocc/4.0.0               gnu-parallel/20230922      nodejs/18.18.2
   aocc/4.1.0        (D)    go/1.21.1                  openjdk/21.0.1
   apptainer/1.2.4          intel-oneapi/2023.2        pgi/19.10.nollvm
   gaussian/g16.C.01        miniconda3/23.5.2.py311    pgi/19.10        (D)
   gcc/13.2.0               miniforge3/24.9.0

  Where:
   D:  Default Module
```

### Install few applications with conda-app and pipx

```sh
module load miniforge3
```

```sh
which conda
```

```sh
conda activate base
pip install conda-app
```

```sh
conda-app install pipx python=3.12

```

```sh
conda-app install mercurial
```

We can open a new session (with ssh) or source the file `~/.bashrc` to get the

```sh
. ~/.bashrc
```

`which hg` should give `~/.local/bin/conda-app-bin/hg`.

Then we can install applications with pipx as described in
https://fluidhowto.readthedocs.io/en/latest/setup-apps.html. For example, for [PDM]:

```sh
pipx install pdm
```

### Create a virtual environment with conda (simple solution)

These commands install few packages in a new environment without compilation.

```sh
module load miniforge3
conda create -n env-fluidsim fluidsim openmpi fluidfft-fftwmpi -y
```

### Compile Fluidsim from source (more advanced)

Here, we show how to use modules to setup an environment to be able to compile packages
from source.

Let us first create an environment with only Python:

```sh
module load miniforge3
conda create -n env-fluidsim-from-src python=3.13 -y
```

We can then activate the environment and load few modules.

```sh
conda activate env-fluidsim-from-src
module load gcc openmpi fftw hdf5
```

The list of modules loaded can be printed with `module list`, which gives

```
Currently Loaded Modules:
  1) miniforge3/24.9.0   2) gcc/13.2.0   3) openmpi/4.1.6   4) fftw/3.3.10   5) hdf5/1.14.2.mpi
```

It can be useful to study a module:

```
module show fftw
module show hdf5/1.14.2.mpi
```

which gives for fftw:

```
$ module show fftw
----------------------------------------------------------------------------------------------------------
   /bxfs/modules/mpi/gcc/13.2.0/openmpi/4.1.6/fftw/3.3.10.lua:
----------------------------------------------------------------------------------------------------------
help([[This module loads the FFTW-3.3.10 library build with gcc-13.2.0 and openmpi/4.1.6
]])
prepend_path("PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/bin")
prepend_path("LIBRARY_PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/lib")
prepend_path("LD_LIBRARY_PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/lib")
prepend_path("INCLUDE","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/include")
prepend_path("CPATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/include")
prepend_path("PKG_CONFIG_PATH","/bxfs/softwares/fftw/3.3.10-gcc.13.2.0-openmpi.4.1.6/lib/pkgconfig")
```

Build and install Python packages with pip.

```
pip cache remove pyfftw; pip install pyfftw --no-binary pyfftw
pip cache remove mpi4py; pip install --no-binary=mpi4py mpi4py

# this should work
# pip cache remove h5py; HDF5_MPI="ON" CC=mpicc pip install --no-binary=h5py h5py
# but with Python 3.13 and h5py<=3.12.1 we actually need (see https://github.com/h5py/h5py/issues/2523)
pip install pkgconfig cython setuptools numpy
pip cache remove h5py; HDF5_MPI="ON" CC=mpicc pip install --no-binary=h5py h5py --no-build-isolation
# to check h5py installation
pip install pytest pytest-mpi
mpirun -np 2 python -c 'import h5py; h5py.run_tests()'
```

Let's clone and install from source fluidsim:

```sh
cd ~
hg clone https://foss.heptapod.net/fluiddyn/fluidsim
cd fluidsim
pip install .[fft,mpi,test]
pip install fluidfft-fftw
```

We can test our fluidsim install in sequential:

```sh
pytest --pyargs fluidsim
```

Finally we need to build and install at least one fluidfft plugin:

```sh
pip install fluidfft-mpi-with-fftw
pip install fluidfft-fftwmpi
```

We can test out install in parallel:

```sh
mpirun -np 2 pytest --pyargs fluidsim
```

## Launch a job with Slurm (manually)

Computations on supercomputers need to be launched with a scheduler. Mesonet and Zen use
Slurm and a nice and simple
[documentation on Slurm](https://www.mesonet.fr/documentation/user-documentation/category/slurm)
is provided.

### Minimal example

We can start by a minimalist example taken from
https://www.mesonet.fr/documentation/user-documentation/code_form/zen/jobs.

Create a temporary directory:

```sh
mkdir -p ~/tmp/example_slurm_sleep
cd ~/tmp/example_slurm_sleep
```

Create a file `job-sleep600.slurm` in this directory containing:

```
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --time=00:10:00

hostname
sleep 600
```

This describes a "job", which can be submitted with `sbatch job-sleep600.slurm`. You can
then study the queue de job and the state of this job with the command `squeue`. This
job can be stopped with `scancel`.

### A Fluidsim simulation

We provide some scripts in the directory
[book/part0/mesonet](https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb/-/tree/main/book/part0/mesonet).
You can clone the repository and enter into this directory with

```sh
cd ~
hg clone https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb.git
cd num-methods-turb/book/part0/mesonet
```

We have a Fluidsim script to run a short simulation:

```{literalinclude} mesonet/simul_ns3d_forced_isotropic.py
---
lineno-match:
---
```

And a file `job-fluidsim.slurm`:

```{literalinclude} mesonet/job-fluidsim.slurm
---
lineno-match:
---
```

You can try to launch it with `sbatch job-fluidsim.slurm`.

## Launch a job with Fluiddyn

It is actually much simpler to use a Python script to write and submit your `.slurm`
scripts.

```{literalinclude} mesonet/submit_job_fluidsim_from_src.py
---
lineno-match:
---
```

which can be used with `./submit_job_fluidsim_from_src.py`. This is very powerfull
because one can adapt the script to launch a lot of simulations!

Finally, here is another example using another environment:

```{literalinclude} mesonet/submit_job_fluidsim.py
---
lineno-match:
---
```

You can try to adapt these scripts to launch different simulations.

[pdm]: https://pdm-project.org/
