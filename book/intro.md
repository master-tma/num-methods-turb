# Course Numerical methods for turbulence

This course is about numerical methods for turbulence. Since it is a huge subject, we
will focus only on few methods for Computational Fluid Dynamics (CFD).

The few first sessions are dedicated to present basic tools that we will use during the
course. The aim is to give essential skills for young scientists working with computers:
using Linux, installing software/tools with conda, using version control and web
platforms like Gitlab/Github.

The first part of the course (presented by
[Christophe Picard](https://membres-ljk.imag.fr/Christophe.Picard/), associate professor
at LJK) is a general reminder of numerical methods and notion of HPC. A practical work
is about building a finite difference solver for 2D Navier-Stokes with application to
Taylor-Green Vortex flow.

The second part of the course (presented by
[Pierre Augier](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/), CNRS researcher
at LEGI) is about pseudo-spectral approaches and 2D turbulence. We will try to
understand the concepts of energy/enstrophy transfers and fluxes with the CDF software
[Fluidsim].

The third part of the course (presented by
[Guillaume Balarac](http://www.legi.grenoble-inp.fr/web/spip.php?auteur57), professor at
LEGI) is about 3D turbulence and turbulence modeling. We will use [Fluidsim] on a
cluster for 3D turbulence to study the spectral energy balance (energy fluxes and
dissipation) and LES modeling.

[fluidsim]: https://fluidsim.readthedocs.io
