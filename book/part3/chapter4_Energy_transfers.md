# Energy transfers and numerical resolution

## Transfers between mean and turbulent fields

From the Reynolds decomposition, any flow-field is decomposed as a mean and a turbulent
(or fluctuating) field:

```{math}
u_i(\vec{x},t) = \langle u_i \rangle(\vec{x}) + u'_i(\vec{x},t)
```

where $\langle \cdot \rangle$ is the temporal averaging operator (which is appropriated
for the flow configuration).

This allows to decompose the mean kinetic energy, $\frac{1}{2} \langle u_i u_i \rangle$
as the kinetic energy of the mean field,
$\frac{1}{2}\langle u_i \rangle \langle u_i \rangle$, and the turbulent kinetic energy
(TKE), $\frac{1}{2} \langle u'_i u'_i \rangle$.

````{exercise}

- Show that:
```{math}
\frac{1}{2} \langle u_i u_i \rangle = \frac{1}{2}\langle u_i \rangle \langle u_i \rangle + \frac{1}{2} \langle u'_i u'_i \rangle
```

- By writting the transport equation for the kinetic energy of the mean field and for the turbulent kinetic energy, explain the main mechanisms of the energy balance in turbulence.
````

## Scale repartion of energy in turbulence

```{exercise}

Explain briefly the Kolmogorov theory and the $k^{-5/3}$ law, and link that with the turbulent kinetic energy (TKE).

```

Keeping the computational domain discretized by $64\times64\times64$ points, perform
additional runs for different viscosity value: $\nu_2 = 0.0028$ and $\nu_3 = 0.001$.

- fix the final time to the simulation at $100 l_*/u_*$
- store solution every $10.0 l_*/u_*$
- compute the global quantities every $0.1 l_*/u_*$
- compute velocity profiles every $1.0 l_*/u_*$
- compute spectra every $1.0 l_*/u_*$

```{exercise}

By observing velocity profile, flow visualization and spectra, explain the differences between theses two cases ($\nu_2 = 0.0028$ and $\nu_3 = 0.001$), and the previous cases ($\nu_1 = 0.0105$)

```
