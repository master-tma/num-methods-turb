# Large-Eddy Simulation (LES) of turbulent flows

## Theoretical part

Lectures notes on LES are available
[here](https://cloud.univ-grenoble-alpes.fr/s/MsCN3Edp9qspaPi).

```{exercise}
Based on these lectures notes (and other materials that you can find), answer the following questions:

- For numerical prediction of turbulent flows, what is the difference between DNS, RANS and LES?
	 
- What is the main idea of LES? Why is relevant?	

- What is the equations to solve to perform LES? 

- What is the role of the SGS tensor for the energetic point of view?

- For modeling of the SGS tensor, what is the functional approach? what is the structural approach?

-  What is the Smagorinsky model (functional approach)?

-  What is the dynamic procedure used to overcome limitation of the Smagorinsky model?
```

## Practice

Update the git project:

```bash
git pull
```

Access to the additional materials for this part:

```bash
cd num-methods-turb/book/part3/practical_chapter5
ls -lrt
```

There are three files:

- `simul_ns3d_kolmo.py`, the input file to simulate Kolmogorov flow with Fluidsim (as in
  previous part)
- `tenibre.run` to launch simulation using slurm ressources manager (as in previous
  part)
- `fluidsim_my_models.py` the model file where the Smagorinsky model will be developped

```{exercise}

Look at and explain the difference between the new `simul_ns3d_kolmo.py` file and the previos one.

```

```{exercise}

Complete the `fluidsim_my_models.py` file to develop the (static) Smagorinsky model, and then perform the LES of the last case of the previous chapter: computational domain discretized by $64\times64\times64$ points, and $\nu_3 = 0.001$.

```
