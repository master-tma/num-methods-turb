# SSH connection, code installation and job management

3D turbulent simulation needs significant computational ressources. This kind of
simulations is performed on supercomputer (using Linux OS). Specific ressources manager
has to be used to launch simulations.

In this first part, the objective is to be able to connect to a supercomputer by using
ssh protocole, to install [Fluidsim] on the supercomputer and to be able to launch run
by using the ressource manager.

## SSH connection

The cluster use for this course is one of the cluster of the [MOST team] of [LEGI]. The
Tenibre cluster is described
[here](http://www.legi.grenoble-inp.fr/web/spip.php?article1395). This is a 64-cores
(Intel Ivy Bridge) composed by 4 nodes with 2 processors with 8 cores each. The
ressource manager [slurm] is used.

Connection to Tenibre cluster is done by ssh protocole.

First, edit the ssh configuration file:

```bash
gedit ~/.ssh/config &
```

and add the lines:

```bash
Host *.vnc
        HostKeyAlgorithms +ssh-rsa
        PubkeyAcceptedKeyTypes +ssh-rsa
        User LOGIN
        ProxyCommand ssh -XC LOGIN@orr.legi.grenoble-inp.fr "nc -w 60 `basename %h .vnc` %p"
        ServerAliveInterval 600
        ServerAliveCountMax 30
```

where **LOGIN** should be replaced by your login.

You should now be able to connect to the Tenibre cluster by using:

```bash
ssh -YC tenibre.vnc
```

```{warning}
The display export should be tested during the session. Try the command `xclock`
```

## Code installation

Fluidsim is already installed on the tenibre-cluster. To load the environment first load
the Mamba module:

```bash
module load Mamba
```

and then activate Fluidsim:

```bash
mamba activate env_fluidsim
```

## Job management

First to access to the additional materials, clone the project on Tenibre:

```bash
cd
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb.git
```

Access to the additional materials for this part:

```bash
cd num-methods-turb/book/part3/practical_chapter1
ls -lrt
```

The file `tenibre.run` is the file allowing the submission of a job on the cluster. The
file contains following lines:

```{eval-rst}
.. literalinclude:: ./practical_chapter1/tenibre.run
   :language: bash
```

The job can be submitted with:

```batch
sbatch tenibre.run
```

A log file is generated:

```sbatch
cat slurm-JOBID.out
```

Other useful command for slurm:

```batch
squeue   ## to see the list of jobs
scancel JOBID ## to cancel a submitted or running job
```

[fluidsim]: https://fluidsim.readthedocs.io
[legi]: http://www.legi.grenoble-inp.fr/web/
[most team]: http://www.legi.grenoble-inp.fr/web/spip.php?rubrique43
[slurm]: https://en.wikipedia.org/wiki/Slurm_Workload_Manager
