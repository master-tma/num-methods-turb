# Direct numerical simulations

Access to the additional materials are the same as the previous part:

```bash
cd num-methods-turb/book/part3/practical_chapter2
ls -lrt
```

## Laminar and turbulent cases

In this part, the code is now used to perform simulations of Kolmogorov flow in various
configurations.

First, go back to the initial set-up

```bash
git checkout simul_ns3d_kolmo.py
```

______________________________________________________________________

For the **first test**:

- fix the final time to the simulation at $40 l_*/u_*$:

```bash
params.time_stepping.t_end = 40.0
```

- store solution every $1.0 l_*/u_*$:

```bash
params.output.periods_save.phys_fields = 1.0
```

- compute the global quantities every $0.1 l_*/u_*$:

```bash
params.output.periods_save.spatial_means = 0.1
```

- compute velocity profiles at the end of the simulation:

```bash
params.output.periods_save.horiz_means = 40.0
```

______________________________________________________________________

For the **second run**:

- change the viscosity in comparison with the **first run**:

```bash
params.nu_2 = 0.0105
```

- perform a simulation until $t_{\rm end}=40 l_*/u_*$
- store solution every $1.0 l_*/u_*$
- compute the global quantities every $0.1 l_*/u_*$
- compute velocity profiles at the end of the simulation

______________________________________________________________________

````{exercise}
Load the solutions in the result folders.

Observe the dynamic of the flow, by using:

```bash
sim.output.phys_fields.set_equation_crosssection("y=0")
sim.output.phys_fields.animate(
    'vx', dt_frame_in_sec=0.5, dt_equations=1.0, tmin=0.0, tmax=40.0, interactive=True
)
```

Observe the evolution of the kinetic energy by using (second plot):

```bash
sim.output.spatial_means.plot()
```

Observe the mean velocity profile at the end of the simulation:

```bash
sim.output.horiz_means.plot(tmin=40, tmax=40)
```

- explain the set-up of the simulation, including initial condition

- explain the difference of behavior between the **first** and the **second** run

````

## Mesh requirement

Only the turbulent case is considered now.

Perform a longer simulation, until $t_{\rm end}=100 l_*/u_*$, and:

- store solution every $10.0 l_*/u_*$
- compute the global quantities every $0.1 l_*/u_*$
- compute velocity profiles every $1.0 l_*/u_*$
- compute spectra every $1.0 l_*/u_*$:

```bash
params.output.periods_save.spectra = 1.0
```

```{exercise}

Based on
[Sarris et al.](https://www.researchgate.net/publication/241271875_Box-size_dependence_and_breaking_of_translational_invariance_in_the_velocity_statistics_computed_from_three-dimensional_turbulent_Kolmogorov_flows)
and on numerical experiments, give two arguments to show that this Kolmogorov flow
configuration solved by using $64\times64\times64$ grid points is indeed a DNS
(Direct Numerical Simulation), i.e. that all the turbulent scales (until the
Kolmogorov scale) are solved.

**Indication**: by computing the Kolmogorov scale using isotropic definition, and
knowing DNS set-up at a given Reynolds number, find a scaling linking the mesh
requirement and the Reynolds number

```
