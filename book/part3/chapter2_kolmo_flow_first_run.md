# Flow configuration and code performance

## Kolmogorov flow

3D Navier-Stokes equation with forcing term:

```{math}
\frac{\partial u_i}{\partial t} +u_j \frac{\partial u_i}{\partial x_j} = - \frac{\partial p}{\partial x_i} + \nu\frac{\partial^2 u_i}{\partial x_j \partial x_j}+f_i
```

with $\vec{f}$ a forcing term.

In this case, $f_x = A sin(k_f z)$ and $f_y=f_z=0$. In all the tests, $A=1$ and $k_f=1$.

see Sarris et al. (_Box-size dependence and breaking of translational invariance in the
velocity statistics computed from three-dimensional turbulent Kolmogorov flows, Phys.
Fluids, 2007_) for
[details](https://www.researchgate.net/publication/241271875_Box-size_dependence_and_breaking_of_translational_invariance_in_the_velocity_statistics_computed_from_three-dimensional_turbulent_Kolmogorov_flows).

````{exercise}

- Considering a laminar permanent Kolmogorov flow, find velocity fields.
- Show that the characteristic Reynolds number can then be defined as:

```{math}
Re = \frac{u_* l_* }{ \nu} = \frac{A^{1/2}}{k_f^{3/2}\nu}.
```

````

## First simulation: code performances

Access to the additional materials for this part:

```bash
cd num-methods-turb/book/part3/practical_chapter2
ls -lrt
```

There are two files:

- `simul_ns3d_kolmo.py`, the input file to simulate Kolmogorov flow with Fluidsim
- `tenibre.run` to launch simulation using slurm ressources manager

Note that for `tenibre.run`, the lines:

```bash
#SBATCH --ntasks=4   # Maximum 16
(...)
mpirun -np 4 python simul_ns3d_kolmo.py
```

mean that the run is launched in parallel by using 4 cores. Only these lines will be
modified to change the number of cores (keeping 16 cores as maximum).

About the file `simul_ns3d_kolmo.py`, lines:

```bash
params.oper.nx = params.oper.ny = nh = 64
params.oper.nz = nh
```

mean that the computational domain is discretized by $64\times64\times64$ points.

The lines:

```bash
params.oper.Lx = params.oper.Ly = Lh = 2.0 * pi
params.oper.Lz = Lh * params.oper.nz / params.oper.nx
```

mean that the computational domain is a cube with length size $2\pi$.

The line:

```bash
params.time_stepping.t_end = 20.0
```

means that the simulation is performed for $20 l_*/u_*$ (i.e. non-dimensional time
units).

Finally, line:

```bash
params.nu_2 = 0.1
```

is to define the viscosity, $\nu=0.1$ here.

````{exercise}

- Run this set-up on 1, 2, 4, 8, and 16 cores and note the restitution time given
  by:
```
Computation completed in
```
in the log file

- Do the same test but for a case composed by $32\times32\times32$ grid points for a total time of $80 l_*/u_*$.

- Do the same test but for a case composed by $16\times16\times16$ grid points for a total time of $160 l_*/u_*$.

- Give an interpretation of the performance in term of restitution time.

- Give an explanation about the difference on the time-step values for the three different grid resolution (mesh size) tested.

````
