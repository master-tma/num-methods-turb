# Numerical methods for Navier-Stokes

finite differences, finite volumes, Smoothed-Particle Hydrodynamics, Lattice Boltzmann,
etc... coupling Pressure-Velocity (Poisson equation)

First stages toward Navier-Stokes using finite differences :
[Jupyter notebook](https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb/-/blob/main/book/part1/navier-stokes-fd.ipynb)
