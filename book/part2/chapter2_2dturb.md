---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# 2D turbulence: inverse and downscale cascades

```{math}
\newcommand{\p}{\partial}
\newcommand{\vv}{\boldsymbol{v}}
\newcommand{\xx}{\boldsymbol{x}}
\newcommand{\kk}{\boldsymbol{k}}
\newcommand{\hvv}{\hat{\boldsymbol{v}}}
\newcommand{\bnabla}{\boldsymbol{\nabla}}
\newcommand{\bomega}{\boldsymbol{\omega}}
\newcommand{\Dt}{\text{D}_t}
\newcommand{\kmax}{k_{\max}}
\newcommand{\sslash}{\mathbin{/\mkern-6mu/}}
\newcommand{\mean}[1]{{\langle #1 \rangle}}
```

## Notes on 3D homogeneous and isotropic turbulence

For simplicity, we will consider only flows with periodic boundary conditions. For such
flows, the space average of any quantities that can be expressed as a divergence is
zero:

```{math}
\mean{\p_i f_i(\xx)} = 0.
```

The Navier-Stokes equations for an incompressible ($\bnabla\cdot \vv = 0$) fluid can be
written as

```{math}
\Dt \vv = -\bnabla p + \nu \bnabla^2 \vv,
```

where $\vv$ is the velocity, $p$ is the rescaled pressure (in m$^2$/s$^{2}$), $\nu$ is
the cinematic viscosity (in m$^2$/s), $\bnabla^2$ is the Laplacian operator and
$\Dt \vv = \p_t \vv + \vv \cdot \bnabla \vv$.

We will also study the dynamics of the vorticity $\bomega = \bnabla \wedge
\vv$:

```{math}
\Dt \bomega = \bomega \cdot \bnabla \vv + \nu \bnabla^2 \bomega,
```

### Conserved quantities

The kinetic energy $E = \mean{|\vv|^2/2}$ is conserved by the nonlinear dynamics:

```{math}
d_t E = d_t \mean{\frac{|\vv|^2}{2}} = ... = - \nu \mean{\p_j v_i \p_j v_i}.
```

There are also other conserved quantities, as for example the helicity
$H = \vv
\cdot \bomega$.

````{important}

The enstrophy is not conserved in 3d. There is a production terms related to
the energy cascade towards small scales and to vorticity stretching.

```{math}
d_t Z = d_t \mean{|\bomega|^2/2} = \mean{\bomega \cdot ( \bomega \cdot \bnabla \vv)} - \nu \mean{\p_j \omega_i \p_j \omega_i}.
```
````

### In spectral space

NS equations in spectral space

Perseval equality for the energy

$E(\kk) = |\hvv|^2 / 2$

```{warning}

$E(\kk)$ is NOT a spectrum!

```

Definition of a spectrum...

#### Spectral energy budget

```{math}
d_t E(\kk) = d_t |\hvv|^2 / 2 = T(\kk) - D(\kk),
```

where $T(\kk) = ...$ and $D(\kk) = ...$.

Nonlinear transfers conserve the energy.

Spectra flux $\Pi(k)$.

#### Consequences of constant energy flux

## 2D turbulence

```{math}
\Dt \omega_z = \nu \bnabla^2 \omega_z,
```

Z is a conserved quantity...

```{math}
Z(\kk) = k^2 E(\kk)
```

Demo...

Direct cascade (towards small scales) is blocked by the conservation of enstrophy and
this equality.

### Your task

- Use the [Fluidsim program](https://fluidsim.readthedocs.io) to obtain different flows
  (2d) with inertial range(s) statistically stationary (forced).

- Use the environment variable `FLUIDSIM_PATH` to control where the simulation data are
  saved. Set it in your `~/.bashrc` file with something like
  `export FLUIDSIM_PATH="..."`

- There is an example script in the directory
  [num-methods-turb/book/part2/examples-fluidsim](https://gricad-gitlab.univ-grenoble-alpes.fr/master-tma/num-methods-turb/-/tree/main/book/part2/examples-fluidsim)
  of this repository. Copy it in your own repository and run it.

- There are also
  [other examples](https://foss.heptapod.net/fluiddyn/fluidsim/-/tree/branch/default/doc/examples)
  in the Fluidsim repository.

- Plot and study the spectra and the spectral energy budget. Make and save figures with
  `sim.output.phys_fields.plot()` and movies with `sim.output.phys_fields.animate`.

- Run different simulations, save the launching and figure scripts in your own
  repository. Commit and push your work often!

- Save figures and potentially .md files (at least few README.md files) to explain what
  you did/found.
