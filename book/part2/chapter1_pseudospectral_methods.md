---
jupytext:
  formats: md:myst
  text_representation:
    extension: .md
    format_name: myst
kernelspec:
  display_name: Python 3
  language: python
  name: python3
---

# Spectral and pseudo-spectral methods

```{math}
\newcommand{\p}{\partial}
\newcommand{\vv}{\boldsymbol{v}}
\newcommand{\xx}{\boldsymbol{x}}
\newcommand{\bnabla}{\boldsymbol{\nabla}}
\newcommand{\Dt}{\text{D}_t}
\newcommand{\kmax}{k_{\max}}
\newcommand{\sslash}{\mathbin{/\mkern-6mu/}}
```

## Generality

When one wants to study a physical system or solve equations, it is useful to represent
the solution in a good basis, adapted to the geometry and to the nature of the system.
One can also purposely choose a system and a geometry which can be represented in a
basis with nice characteristics. The spectral and pseudo-spectral methods are based on
these ideas. They are very efficient for problems in quite simple geometries.

One can also decompose a more complicated problem in subproblems that can be represented
in a nice basis. This is the principal of spectral elements methods for which the whole
numerical domain is decomposed in small elements on which one can apply spectral
methods.

The conditions to (partly or completely) represent a solution on a spectral basis can
be:

- the geometry should be describable separately for the different dimensions (like for
  example for a rectangle or a cylinder)

- at least one dimension has to be characterized by

  - periodic boundary conditions (then the Fourier basis is perfect)

  - condition of 0 value at the boundary (then one can use bases using sinus or
    [Chebyshev polynomials](https://en.wikipedia.org/wiki/Chebyshev_polynomials))

## Periodical boundary conditions and Fourier basis

### One dimension case

In the case of periodical boundary conditions, the Fourier base is of course perfectly
adapted.

Let us take the example of the one dimensional
[Burger equation](https://fr.wikipedia.org/wiki/%C3%89quation_de_Burgers)

```{math}
---
label: burger-eq
---
\p_t v + v \p_x v = \nu \p_x^2 v,
```

where $v(x, t)$ is the velocity and $\nu$ is the viscosity. We see that this equation is
similar to the compressible Navier-Stokes equations, but without the pressure term. We
will discuss how to solve this equation on a numerical domain of length $L_x$ with $n_x$
grid points. We define the Fourier transform as

```{math}
TF[v] = \hat v(k_x, t) = \int_0^{L_x} \frac{dx}{L_x} v(x, t) e^{-i k_x x},
```

and the inverse Fourier transform as

```{math}
TF^{-1}[\hat v] = \sum_{k_x} \hat v e^{i k_x x}.
```

It is easy to show that with these definitions $TF^{-1}[TF[v]] = v$.

The locations of the grid points are:

```{code-cell} ipython3
import numpy as np
from math import pi

Lx = 2 * pi
nx = 8

dx = Lx / nx

# the s in the name `xs` indicates that this array contains the different x values
# it's common in Python to use plurial for such variables
xs = dx * np.arange(nx)
xs / pi
```

The step in Fourier space is related to the length of the domain:

```{code-cell} ipython3
delta_kx = 2 * pi / Lx
delta_kx
```

and the maximum wavenumber is related to the number of grid points:

```{code-cell} ipython3
kx_max = delta_kx * (nx // 2)
kx_max
```

For most FFT libraries, the wavenumbers are ordered like this:

```{math}
k_x = \delta_{kx} [0, 1, 2, ..., (n_x \sslash 2) - 1, n_x \sslash 2, -(n_x \sslash 2) + 1, ..., -2, -1]
```

In Python-numpy we can write for example (see
[the help of `np.r_`](https://numpy.org/doc/stable/reference/generated/numpy.r_.html))

```{code-cell} ipython3
ks = delta_kx * np.r_[0:nx//2 + 1, -nx//2 + 1:0]
ks
```

#### Using the property of real to complex Fourier transform

When the initial variable in physical space is real, one can use the property of real to
complex (`r2c`) Fourier transform, i.e. the fact that $\hat v(-k_x) = \hat
v(k_x)^*$.
For `r2c` transforms, only values corresponding to wavenumbers $k_x
\geq 0$ are stored,
which implies that, unsurprisingly, the memory footprint is nearly the same in physical
and complex spaces. The wavenumber array is then just:

```{code-cell} ipython3
ks = delta_kx * np.arange(nx//2 + 1)
ks
```

#### Equation in the Fourier basis

Applying the FT of {eq}`burger-eq`, we get

```{math}

\p_t \hat v = - TF[v \p_x v] - \nu {k_x}^2 \hat v,

```

We see that in the Fourier basis, the linear terms involving spatial operators are
diagonal and thus very easy to compute exactly. This is the first huge advantage of
spectral methods.

#### How to compute non linear terms

We need to be able to compute the Fourier transform of non linear terms like $v\p_x
v$
as a function of Fourier transform of $v$.

Mathematically, we know that the Fourier transform of a product is the convolution of
the Fourier transforms of the 2 terms:

```{math}
TF[f g] = \hat f \ast \hat g.
```

However, the computation of the convolution is numerically very expensive and there is a
better solution, which is to compute the product in physical space:

```{math}
TF[v \p_x v] = TF[ TF^{-1}[ \hat v ]  TF^{-1}[ i k_x \hat v ] ].
```

This solution involves a lot of Fourier transport and programs using such
"**pseudo-spectral**" methods spend a lot of time just doing spectral transforms.

It is still more efficient that the convolution since there exists the Fast Fourier
Transform (FFT) algorithm. This algorithm has a complexity scaling like
$\mathcal{O}(n_x \log{n_x})$, i.e. nearly linear in the number of grid point! Since
there exists very efficient implementations of this algorithms (in particular the well
known [FFTW](https://www.fftw.org/) library), pseudo spectral methods are very
competitive in terms of performance, especially for large resolutions.

```{admonition} Multidimensional FFT

First one `r2c` transform on the dimension contiguous in memory and then other
`c2c` transform(s). This just complicates the memory layout and the
implementation of the FFT.

```

```{admonition} Aliasing and dealiasing methods
Mostly "truncation", but other methods exist.
```

#### Notes on parallelization of FFT algorithms

There are two very different classes of methods to parallelize CPU bounded computations:

- Using threads (with or without OpenMP) within one process with **shared memory**. Such
  methods can be very good to parallelize a computation over 1 unique computer.

- Using different processes with **distributed memory** and messages between the process
  using for example the
  [MPI protocol](https://en.wikipedia.org/wiki/Message_Passing_Interface). Such methods
  have to be used to parallelize code over more than one node (on clusters for example).

Unfortunately, there is no efficient algorithm to parallelize 1d FFT with **distributed
memory**. There are many libraries for parallel FFT. The CFD framework [Fluidsim] takes
advantage of the library [Fluidfft] to use different FFT library with one unique Python
API.

[fluidfft]: https://fluidfft.readthedocs.io
[fluidsim]: https://fluidsim.readthedocs.io
